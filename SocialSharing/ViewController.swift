//
//  ViewController.swift
//  SocialSharing
//
//  Created by Diego Caridei on 15/10/14.
//  Copyright (c) 2014 Diego Caridei. All rights reserved.
//

import UIKit
import Social
class ViewController: UIViewController {

    
    @IBAction func facebook(sender: AnyObject) {
        // controlliamo se è presente qualche account Facebook sul dispositivo
        if SLComposeViewController.isAvailableForServiceType(SLServiceTypeFacebook) {
            //Creiamo un oggetto SLComposeViewController
            var controller = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
            //inizializziamo il tutto con un messaggio
            controller.setInitialText("#iProg.it tutorial sul framework social")
            //mostriamo la view
            self.presentViewController(controller, animated:true, completion:nil)
        }
        else {
            println("Nessun account Facebook trovato")
        }
    }
    
    //il funzionamento è analogo di quello per Facebook
    @IBAction func twitter(sender: AnyObject) {
        if SLComposeViewController.isAvailableForServiceType(SLServiceTypeTwitter) {
            var controller = SLComposeViewController(forServiceType: SLServiceTypeTwitter)
            controller.setInitialText("#iProg.it tutorial sul framework social")
            
            self.presentViewController(controller, animated:true, completion:nil)
        }
        else {

            println("Nessun account Twitter trovato")
        }

    }
    //Codice di Default
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

